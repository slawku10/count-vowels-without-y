public class Vowels {
    public static int getCount(String string) {
        return (int) string
                .chars()
                .filter(c -> "aeoiu".indexOf(c) >= 0)
                .count();
    }
}
